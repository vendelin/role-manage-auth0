package handler

import (
	"context"
	"net/http"
	"strings"

	"role-manage-auth0/internal/auth0-management/api"

	"github.com/auth0/go-auth0/management"
)

// AssignPermissions is a http handler for adding auth0 permissions to a role.
func (s *AuthService) AssignPermissions(ctx context.Context,
	req api.AssignPermissionsRequestObject,
) (api.AssignPermissionsResponseObject, error) {
	perms := s.permissionsFromRequest(req.Params.Permissions)
	err := s.auth0API.Role.AssociatePermissions(req.RoleId, perms, management.Context(ctx))
	if err != nil {
		return api.AssignPermissionsdefaultJSONResponse{
			StatusCode: http.StatusInternalServerError,
			Body:       showError(err),
		}, nil
	}
	return &api.AssignPermissions200JSONResponse{}, nil
}

// ListRolePermissions is a http handler for listing auth0 permissions by a specific role.
func (s *AuthService) ListRolePermissions(ctx context.Context,
	req api.ListRolePermissionsRequestObject,
) (api.ListRolePermissionsResponseObject, error) {
	opts := paging(ctx, api.ListUserRolesParams{Page: req.Params.Page, PerPage: req.Params.PerPage})
	l, err := s.auth0API.Role.Permissions(req.RoleId, opts...)
	if err != nil {
		return api.ListRolePermissionsdefaultJSONResponse{
			StatusCode: http.StatusInternalServerError,
			Body:       showError(err),
		}, nil
	}
	return api.ListRolePermissions200JSONResponse(permissionToResponse(l.Permissions)), nil
}

// ListUserPermissions is a http handler for listing auth0 permissions by a specific user.
func (s *AuthService) ListUserPermissions(ctx context.Context,
	req api.ListUserPermissionsRequestObject,
) (api.ListUserPermissionsResponseObject, error) {
	opts := paging(ctx, api.ListUserRolesParams{Page: req.Params.Page, PerPage: req.Params.PerPage})
	l, err := s.auth0API.User.Permissions(req.UserId, opts...)
	if err != nil {
		return api.ListUserPermissionsdefaultJSONResponse{
			StatusCode: http.StatusInternalServerError,
			Body:       showError(err),
		}, nil
	}
	return api.ListUserPermissions200JSONResponse(permissionToResponse(l.Permissions)), nil
}

// RemovePermissions is a http handler for removing auth0 permissions from a role.
func (s *AuthService) RemovePermissions(ctx context.Context,
	req api.RemovePermissionsRequestObject,
) (api.RemovePermissionsResponseObject, error) {
	perms := s.permissionsFromRequest(req.Params.Permissions)
	err := s.auth0API.Role.RemovePermissions(req.RoleId, perms, management.Context(ctx))
	if err != nil {
		return api.RemovePermissionsdefaultJSONResponse{
			StatusCode: http.StatusInternalServerError,
			Body:       showError(err),
		}, nil
	}
	return &api.RemovePermissions200Response{}, nil
}

func (s *AuthService) permissionsFromRequest(ps string) []*management.Permission {
	permNames := strings.Split(ps, ",")
	perms := make([]*management.Permission, len(permNames))
	for i, permName := range permNames {
		permName := permName
		perms[i] = &management.Permission{Name: &permName, ResourceServerIdentifier: s.resServer}
	}
	return perms
}

func permissionToResponse(ps []*management.Permission) api.PermissionsResponse {
	res := make(api.PermissionsResponse, len(ps))
	for i, p := range ps {
		res[i] = api.PermissionResponse{Name: p.Name, Description: p.Description}
	}
	return res
}
