package handler

import (
	"net/http"

	"role-manage-auth0/internal/auth0-management/api"
	"role-manage-auth0/pkg/util"

	middleware "github.com/deepmap/oapi-codegen/pkg/gin-middleware"
	"github.com/getkin/kin-openapi/openapi3"
	"github.com/gin-gonic/gin"
	openapi "github.com/go-openapi/runtime/middleware"
)

const (
	openapiDir  = "api/openapi"
	openapiFile = "auth0-management.yaml"
)

// New returns a http handler for swagger ui of the generated open api.
func New() http.Handler {
	swagger, err := api.GetSwagger()
	util.Must("loading swagger", err)

	h := gin.Default()
	registerSwaggerRoute(h)
	basePath := fixupSwagger(swagger)
	h.Use(middleware.OapiRequestValidator(swagger))
	strictAPIHandler := api.NewStrictHandler(newAuthService(), nil)
	api.RegisterHandlersWithOptions(h, strictAPIHandler, api.GinServerOptions{BaseURL: basePath})
	return h
}

// fixupSwagger fixes swagger paths, and returns server base path.
func fixupSwagger(swagger *openapi3.T) string {
	basePath, err := swagger.Servers.BasePath()
	util.Must("get basepath from servers", err)

	// Clear out the servers array in the swagger spec, that skips validating
	// that server names match. We don't know how this thing will be run.
	swagger.Servers = nil

	// Fix bug in code generator: https://stackoverflow.com/a/70164132/6155997
	updatedPaths := make(openapi3.Paths)
	for key, value := range swagger.Paths {
		updatedPaths[basePath+key] = value
	}
	swagger.Paths = updatedPaths

	return basePath
}

func registerSwaggerRoute(g *gin.Engine) {
	g.Static(openapiDir, openapiDir)
	opts := openapi.SwaggerUIOpts{SpecURL: openapiDir + "/" + openapiFile}
	sh := openapi.SwaggerUI(opts, nil)
	g.GET("/docs", gin.WrapH(sh))
}
