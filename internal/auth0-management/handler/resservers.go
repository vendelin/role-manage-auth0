package handler

import (
	"context"
	"net/http"
	"strings"

	"role-manage-auth0/internal/auth0-management/api"

	"github.com/auth0/go-auth0/management"
)

// ReadResServer is a http handler for getting a resource server by id.
func (s *AuthService) ReadResServer(ctx context.Context,
	req api.ReadResServerRequestObject,
) (api.ReadResServerResponseObject, error) {
	r, err := s.auth0API.ResourceServer.Read(req.ResserverId, management.Context(ctx))
	if err != nil {
		return api.ReadResServerdefaultJSONResponse{
			StatusCode: http.StatusInternalServerError,
			Body:       showError(err),
		}, nil
	}
	res := api.ReadResServer200JSONResponse(resServerToResponse(r))
	return &res, nil
}

// ListResServers is a http handler for listing auth0 resServers by optional filter and sort fields.
func (s *AuthService) ListResServers(ctx context.Context,
	req api.ListResServersRequestObject,
) (api.ListResServersResponseObject, error) {
	rp := req.Params
	opts := paging(ctx, api.ListUserRolesParams{Page: req.Params.Page, PerPage: req.Params.PerPage})
	if rp.IncludeFields != nil {
		opts = append(opts, management.IncludeFields(strings.Split(*rp.IncludeFields, ",")...))
	}
	l, err := s.auth0API.ResourceServer.List(opts...)
	if err != nil {
		return api.ListResServersdefaultJSONResponse{
			StatusCode: http.StatusInternalServerError,
			Body:       showError(err),
		}, nil
	}
	return api.ListResServers200JSONResponse(resServersToResponse(l.ResourceServers)), nil
}

// EditResServer is a http handler for updating a resource server by id.
func (s *AuthService) EditResServer(ctx context.Context,
	req api.EditResServerRequestObject,
) (api.EditResServerResponseObject, error) {
	r, err := s.auth0API.ResourceServer.Read(req.ResserverId, management.Context(ctx))
	if err != nil {
		return api.EditResServerdefaultJSONResponse{
			StatusCode: http.StatusInternalServerError,
			Body:       showError(err),
		}, nil
	}
	r.ID = nil
	r.Identifier = nil
	b := req.Body
	if len(*b.Scopes) == 0 {
		r.Scopes = &[]management.ResourceServerScope{}
	} else {
		r.Scopes = scopesFromRequest(*b.Scopes)
	}
	if err = s.auth0API.ResourceServer.Update(req.ResserverId, r, management.Context(ctx)); err != nil {
		return api.EditResServerdefaultJSONResponse{
			StatusCode: http.StatusInternalServerError,
			Body:       showError(err),
		}, nil
	}
	res := api.EditResServer200JSONResponse(resServerToResponse(r))
	return &res, nil
}

func resServerToResponse(r *management.ResourceServer) api.ResServerResponse {
	return api.ResServerResponse{
		Id: r.ID, Name: r.Name, Identifier: r.Identifier, Scopes: marshalField("scopes", r.Scopes),
	}
}

func resServersToResponse(rs []*management.ResourceServer) api.ResServersResponse {
	res := make(api.ResServersResponse, len(rs))
	for i, r := range rs {
		res[i] = resServerToResponse(r)
	}
	return res
}

func scopesFromRequest(rs string) *[]management.ResourceServerScope {
	scopePerms := strings.Split(rs, ",")
	scopes := make([]management.ResourceServerScope, len(scopePerms))
	for i, scopePerm := range scopePerms {
		scopePerm := scopePerm
		scopes[i] = management.ResourceServerScope{Value: &scopePerm, Description: &scopePerm}
	}
	return &scopes
}
