package handler

import (
	"context"
	"net/http"
	"strings"

	"role-manage-auth0/internal/auth0-management/api"

	"github.com/auth0/go-auth0/management"
)

// NewRole is a http handler for creating a new role in auth0.
func (s *AuthService) NewRole(ctx context.Context,
	req api.NewRoleRequestObject,
) (api.NewRoleResponseObject, error) {
	r := &management.Role{
		Description: &req.Body.Description,
		Name:        &req.Body.Name,
	}
	if err := s.auth0API.Role.Create(r, management.Context(ctx)); err != nil {
		return api.NewRoledefaultJSONResponse{
			StatusCode: http.StatusInternalServerError,
			Body:       showError(err),
		}, nil
	}
	res := api.NewRole200JSONResponse(roleToResponse(r))
	return &res, nil
}

// ReadRole is a http handler for getting a role by id.
func (s *AuthService) ReadRole(ctx context.Context,
	req api.ReadRoleRequestObject,
) (api.ReadRoleResponseObject, error) {
	r, err := s.auth0API.Role.Read(req.RoleId, management.Context(ctx))
	if err != nil {
		return api.ReadRoledefaultJSONResponse{
			StatusCode: http.StatusInternalServerError,
			Body:       showError(err),
		}, nil
	}
	res := api.ReadRole200JSONResponse(roleToResponse(r))
	return &res, nil
}

// ListRoles is a http handler for listing auth0 roles by optional filter fields.
func (s *AuthService) ListRoles(ctx context.Context,
	req api.ListRolesRequestObject,
) (api.ListRolesResponseObject, error) {
	rp := req.Params
	opts := paging(ctx, api.ListUserRolesParams{Page: req.Params.Page, PerPage: req.Params.PerPage})
	if rp.NameFilter != nil {
		opts = append(opts, management.Parameter("name_filter", *rp.NameFilter))
	}
	l, err := s.auth0API.Role.List(opts...)
	if err != nil {
		return api.ListRolesdefaultJSONResponse{
			StatusCode: http.StatusInternalServerError,
			Body:       showError(err),
		}, nil
	}
	return api.ListRoles200JSONResponse(rolesToResponse(l.Roles)), nil
}

// EditRole is a http handler for updating a role by id.
func (s *AuthService) EditRole(ctx context.Context,
	req api.EditRoleRequestObject,
) (api.EditRoleResponseObject, error) {
	r, err := s.auth0API.Role.Read(req.RoleId, management.Context(ctx))
	if err != nil {
		return api.EditRoledefaultJSONResponse{
			StatusCode: http.StatusInternalServerError,
			Body:       showError(err),
		}, nil
	}
	b := req.Body
	r.ID = nil
	r.Description = oldOrChangedStr(r.Description, b.Description)
	r.Name = oldOrChangedStr(r.Name, b.Name)
	if err = s.auth0API.Role.Update(req.RoleId, r, management.Context(ctx)); err != nil {
		return api.EditRoledefaultJSONResponse{
			StatusCode: http.StatusInternalServerError,
			Body:       showError(err),
		}, nil
	}
	res := api.EditRole200JSONResponse(roleToResponse(r))
	return &res, nil
}

// DeleteRole is a http handler for removing a role by id.
func (s *AuthService) DeleteRole(ctx context.Context,
	req api.DeleteRoleRequestObject,
) (api.DeleteRoleResponseObject, error) {
	if err := s.auth0API.Role.Delete(req.RoleId, management.Context(ctx)); err != nil {
		return api.DeleteRoledefaultJSONResponse{
			StatusCode: http.StatusInternalServerError,
			Body:       showError(err),
		}, nil
	}
	return &api.DeleteRole200Response{}, nil
}

func roleToResponse(r *management.Role) api.RoleResponse {
	return api.RoleResponse{Description: r.Description, Id: r.ID, Name: r.Name}
}

func rolesToResponse(rs []*management.Role) api.RolesResponse {
	res := make(api.RolesResponse, len(rs))
	for i, r := range rs {
		res[i] = roleToResponse(r)
	}
	return res
}

func rolesFromRequest(rs string) []*management.Role {
	roleIDs := strings.Split(rs, ",")
	roles := make([]*management.Role, len(roleIDs))
	for i, roleID := range roleIDs {
		roleID := roleID
		roles[i] = &management.Role{ID: &roleID}
	}
	return roles
}
