package handler

import (
	"context"
	"encoding/json"
	"log"
	"regexp"

	"role-manage-auth0/internal/auth0-management/api"

	"github.com/auth0/go-auth0/management"
)

// reSplitErr splits an (error_code error_message) to parts.
var reSplitErr = regexp.MustCompile(`(\d+) (.*)`)

// marshalField jsn encodes a given value, ad logs any error.
func marshalField(descr string, m interface{}) *string {
	bs, err := json.Marshal(m)
	if err != nil {
		log.Printf("failed to encode %s: %v\n", descr, err)
	}
	result := string(bs)
	if result == "null" {
		return nil
	}
	return &result
}

// unmarshalStringMap json decodes a string key map as a metadata, and logs any error.
func unmarshalStringMap(descr string, content *string) *map[string]interface{} {
	if content == nil {
		return nil
	}
	result := map[string]interface{}{}
	if err := json.Unmarshal([]byte(*content), &result); err != nil {
		log.Printf("failed to encode %s: %v\n", descr, err)
		return nil
	}
	return &result
}

// addOptParamStr adds a query parameter with given key if the value is not nil.
func addOptParamStr(opts []management.RequestOption, value *string, key string,
) []management.RequestOption {
	if value != nil {
		opts = append(opts, management.Parameter(key, *value))
	}
	return opts
}

// oldOrChangedStr replaces metadata value with a new one if the new one is not nil.
func oldOrChangedMeta(descr string, old *map[string]interface{}, newM *string,
) *map[string]interface{} {
	if result := unmarshalStringMap(descr, newM); result != nil {
		return result
	}
	return old
}

// oldOrChangedStr replaces bool value with a new one if the new one is not nil.
func oldOrChangedBool(old, newB *bool) *bool {
	if newB != nil {
		return newB
	}
	return old
}

// oldOrChangedStr replaces string value with a new one if the new one is not nil.
func oldOrChangedStr(old, newS *string) *string {
	if newS != nil {
		return newS
	}
	return old
}

// showError puts auth0 error message to response error type.
func showError(err error) api.Error {
	msg := err.Error()
	code := "1"
	if res := reSplitErr.FindAllStringSubmatch(msg, -1); len(res) > 0 {
		code = res[0][1]
		msg = res[0][2]
	}
	return api.Error{Code: code, Message: &msg}
}

// paging adds page as page index and per page as page size to the request.
func paging(ctx context.Context, pg api.ListUserRolesParams) []management.RequestOption {
	opts := []management.RequestOption{management.Context(ctx)}
	if pg.Page != nil && *pg.Page != 0 {
		opts = append(opts, management.Page(*pg.Page))
	}
	if pg.PerPage != nil && *pg.PerPage != 0 {
		opts = append(opts, management.PerPage(*pg.PerPage), management.Take(*pg.PerPage))
	}
	return opts
}
