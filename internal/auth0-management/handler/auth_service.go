package handler

import (
	"os"

	"role-manage-auth0/pkg/util"

	"github.com/auth0/go-auth0/management"
	"github.com/joho/godotenv"
)

// AuthService implements api.StrictServerInterface for handling users, roles and permissions.
type AuthService struct {
	auth0API  *management.Management
	resServer *string
}

func newAuthService() *AuthService {
	util.Must("load godot", godotenv.Load())
	clientID := os.Getenv("AUTH0_CLIENT_ID")
	clientSecret := os.Getenv("AUTH0_CLIENT_SECRET")
	auth0API, err := management.New(
		os.Getenv("AUTH0_DOMAIN"),
		management.WithClientCredentials(clientID, clientSecret),
	)
	util.Must("initiating auth0 management API", err)
	resServer := os.Getenv("AUTH0_DEFAULT_RESOURCE_SERVER")
	return &AuthService{auth0API, &resServer}
}
