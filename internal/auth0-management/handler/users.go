package handler

import (
	"context"
	"net/http"
	"strings"

	"role-manage-auth0/internal/auth0-management/api"
	"role-manage-auth0/pkg/util"

	"github.com/auth0/go-auth0/management"
)

const passLen = 15

// NewUser is a http handler for creating a new user in auth0.
func (s *AuthService) NewUser(ctx context.Context,
	req api.NewUserRequestObject,
) (api.NewUserResponseObject, error) {
	u := &management.User{
		Connection: util.StrP("Username-Password-Authentication"),
		Email:      &req.Body.Email,
		Password:   util.StrP(util.RandString(passLen)),
	}
	if err := s.auth0API.User.Create(u, management.Context(ctx)); err != nil {
		return api.NewUserdefaultJSONResponse{
			StatusCode: http.StatusInternalServerError,
			Body:       showError(err),
		}, nil
	}
	res := api.NewUser200JSONResponse(userToResponse(u))
	return &res, nil
}

// ReadUser is a http handler for getting a user by id.
func (s *AuthService) ReadUser(ctx context.Context,
	req api.ReadUserRequestObject,
) (api.ReadUserResponseObject, error) {
	u, err := s.auth0API.User.Read(req.UserId, management.Context(ctx))
	if err != nil {
		return api.ReadUserdefaultJSONResponse{
			StatusCode: http.StatusInternalServerError,
			Body:       showError(err),
		}, nil
	}
	res := api.ReadUser200JSONResponse(userToResponse(u))
	return &res, nil
}

// ListUsers is a http handler for listing auth0 users by optional filter and sort fields.
func (s *AuthService) ListUsers(ctx context.Context,
	req api.ListUsersRequestObject,
) (api.ListUsersResponseObject, error) {
	rp := req.Params
	opts := paging(ctx, api.ListUserRolesParams{Page: req.Params.Page, PerPage: req.Params.PerPage})
	if rp.IncludeFields != nil {
		if rp.ExcludeFields != nil {
			return api.ListUsersdefaultJSONResponse{
				StatusCode: http.StatusInternalServerError, Body: api.Error{
					Code:    "1",
					Message: util.StrP("You can set only include or exclude fields."),
				},
			}, nil
		}
		opts = append(opts, management.IncludeFields(strings.Split(*rp.IncludeFields, ",")...))
	} else if rp.ExcludeFields != nil {
		opts = append(opts, management.ExcludeFields(strings.Split(*rp.ExcludeFields, ",")...))
	}
	opts = addOptParamStr(opts, rp.Sort, "sort")
	if rp.Q != nil {
		opts = append(opts, management.Query(*rp.Q))
	}
	l, err := s.auth0API.User.List(opts...)
	if err != nil {
		return api.ListUsersdefaultJSONResponse{
			StatusCode: http.StatusInternalServerError,
			Body:       showError(err),
		}, nil
	}
	return api.ListUsers200JSONResponse(usersToResponse(l.Users)), nil
}

// EditUser is a http handler for updating a user by id.
func (s *AuthService) EditUser(ctx context.Context,
	req api.EditUserRequestObject,
) (api.EditUserResponseObject, error) {
	u, err := s.auth0API.User.Read(req.UserId, management.Context(ctx))
	if err != nil {
		return api.EditUserdefaultJSONResponse{
			StatusCode: http.StatusInternalServerError,
			Body:       showError(err),
		}, nil
	}
	b := req.Body
	u.Identities = nil
	u.UpdatedAt = nil
	u.CreatedAt = nil
	u.ID = nil
	u.LoginsCount = nil
	u.LastIP = nil
	u.LastLogin = nil
	u.LastPasswordReset = nil
	u.AppMetadata = oldOrChangedMeta("app metadata", u.AppMetadata, b.AppMetadata)
	u.Blocked = oldOrChangedBool(u.Blocked, b.Blocked)
	u.Email = oldOrChangedStr(nil, b.Email)
	u.EmailVerified = oldOrChangedBool(nil, b.EmailVerified)
	u.FamilyName = oldOrChangedStr(u.FamilyName, b.FamilyName)
	u.GivenName = oldOrChangedStr(u.GivenName, b.GivenName)
	u.Name = oldOrChangedStr(u.Name, b.Name)
	u.Nickname = oldOrChangedStr(u.Nickname, b.Nickname)
	u.Password = oldOrChangedStr(u.Password, b.Password)
	u.PhoneNumber = oldOrChangedStr(u.PhoneNumber, b.PhoneNumber)
	u.PhoneVerified = oldOrChangedBool(u.PhoneVerified, b.PhoneVerified)
	u.Picture = oldOrChangedStr(u.Picture, b.Picture)
	u.Username = oldOrChangedStr(u.Username, b.Username)
	u.UserMetadata = oldOrChangedMeta("user metadata", u.UserMetadata, b.UserMetadata)
	u.VerifyEmail = oldOrChangedBool(u.VerifyEmail, b.VerifyEmail)
	if err = s.auth0API.User.Update(req.UserId, u, management.Context(ctx)); err != nil {
		return api.EditUserdefaultJSONResponse{
			StatusCode: http.StatusInternalServerError,
			Body:       showError(err),
		}, nil
	}
	res := api.EditUser200JSONResponse(userToResponse(u))
	return &res, nil
}

// DeleteUser is a http handler for removing a user by id.
func (s *AuthService) DeleteUser(ctx context.Context,
	req api.DeleteUserRequestObject,
) (api.DeleteUserResponseObject, error) {
	if err := s.auth0API.User.Delete(req.UserId, management.Context(ctx)); err != nil {
		return api.DeleteUserdefaultJSONResponse{
			StatusCode: http.StatusInternalServerError,
			Body:       showError(err),
		}, nil
	}
	return &api.DeleteUser200Response{}, nil
}

func userToResponse(u *management.User) api.UserResponse {
	return api.UserResponse{
		AppMetadata: marshalField("app metadata", u.AppMetadata), Blocked: u.Blocked,
		ClientId: u.ClientID, Connection: u.Connection, CreatedAt: u.CreatedAt,
		Description: u.Description, Email: u.Email, EmailVerified: u.EmailVerified,
		FamilyName: u.FamilyName, GivenName: u.GivenName, Id: u.ID,
		Identities: marshalField("identities", u.Identities), LastIp: u.LastIP,
		LastLogin: u.LastLogin, LastPasswordReset: u.LastPasswordReset, Location: u.Location,
		LoginsCount: u.LoginsCount, Multifactor: u.Multifactor, Name: u.Name, Nickname: u.Nickname,
		PhoneNumber: u.PhoneNumber, PhoneVerified: u.PhoneVerified, Picture: u.Picture,
		ScreenName: u.ScreenName, UpdatedAt: u.UpdatedAt, Url: u.URL,
		UserMetadata: marshalField("user metadata", u.UserMetadata), Username: u.Username,
		VerifyEmail: u.VerifyEmail,
	}
}

func usersToResponse(us []*management.User) api.UsersResponse {
	res := make(api.UsersResponse, len(us))
	for i, u := range us {
		res[i] = userToResponse(u)
	}
	return res
}

func usersFromRequest(us string) []*management.User {
	userIDs := strings.Split(us, ",")
	users := make([]*management.User, len(userIDs))
	for i, userID := range userIDs {
		userID := userID
		users[i] = &management.User{ID: &userID}
	}
	return users
}
