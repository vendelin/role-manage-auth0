package handler

import (
	"context"
	"net/http"

	"role-manage-auth0/internal/auth0-management/api"

	"github.com/auth0/go-auth0/management"
)

// ListUserRoles is a http handler for listing auth0 roles by a specific user.
func (s *AuthService) ListUserRoles(ctx context.Context,
	req api.ListUserRolesRequestObject,
) (api.ListUserRolesResponseObject, error) {
	opts := paging(ctx, req.Params)
	l, err := s.auth0API.User.Roles(req.UserId, opts...)
	if err != nil {
		return api.ListUserRolesdefaultJSONResponse{
			StatusCode: http.StatusInternalServerError,
			Body:       showError(err),
		}, nil
	}
	return api.ListUserRoles200JSONResponse(rolesToResponse(l.Roles)), nil
}

// ListRoleUsers is a http handler for listing auth0 users by a specific role.
func (s *AuthService) ListRoleUsers(ctx context.Context,
	req api.ListRoleUsersRequestObject,
) (api.ListRoleUsersResponseObject, error) {
	rp := req.Params
	opts := paging(ctx, api.ListUserRolesParams{Page: req.Params.Page, PerPage: req.Params.PerPage})
	if rp.From != nil {
		opts = append(opts, management.From(*rp.From))
	}
	l, err := s.auth0API.Role.Users(req.RoleId, opts...)
	if err != nil {
		return api.ListRoleUsersdefaultJSONResponse{
			StatusCode: http.StatusInternalServerError,
			Body:       showError(err),
		}, nil
	}
	return api.ListRoleUsers200JSONResponse(usersToResponse(l.Users)), nil
}

// AssignRoleUsers is a http handler for adding auth0 role to users.
func (s *AuthService) AssignRoleUsers(ctx context.Context,
	req api.AssignRoleUsersRequestObject,
) (api.AssignRoleUsersResponseObject, error) {
	users := usersFromRequest(*req.Body.Users)
	err := s.auth0API.Role.AssignUsers(req.RoleId, users, management.Context(ctx))
	if err != nil {
		return api.AssignRoleUsersdefaultJSONResponse{
			StatusCode: http.StatusInternalServerError,
			Body:       showError(err),
		}, nil
	}
	return &api.AssignRoleUsers200JSONResponse{}, nil
}

// AssignUserRoles is a http handler for adding auth0 roles to user.
func (s *AuthService) AssignUserRoles(ctx context.Context,
	req api.AssignUserRolesRequestObject,
) (api.AssignUserRolesResponseObject, error) {
	roles := rolesFromRequest(*req.Body.Roles)
	err := s.auth0API.User.AssignRoles(req.UserId, roles, management.Context(ctx))
	if err != nil {
		return api.AssignUserRolesdefaultJSONResponse{
			StatusCode: http.StatusInternalServerError,
			Body:       showError(err),
		}, nil
	}
	return &api.AssignUserRoles200JSONResponse{}, nil
}

// RemoveRoles is a http handler for removing auth0 roles from a role.
func (s *AuthService) RemoveRoles(ctx context.Context,
	req api.RemoveRolesRequestObject,
) (api.RemoveRolesResponseObject, error) {
	roles := rolesFromRequest(req.Params.Roles)
	err := s.auth0API.User.RemoveRoles(req.UserId, roles, management.Context(ctx))
	if err != nil {
		return api.RemoveRolesdefaultJSONResponse{
			StatusCode: http.StatusInternalServerError,
			Body:       showError(err),
		}, nil
	}
	return &api.RemoveRoles200Response{}, nil
}
