package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"net/http"
	"os/signal"
	"sync"
	"syscall"
	"time"

	"role-manage-auth0/internal/auth0-management/handler"
)

const (
	defaultPort   = 8080
	shutdownLimit = 5 * time.Second
	readTimeout   = 30 * time.Second
)

func main() {
	port := flag.Int("port", defaultPort, "Port for HTTP server")
	flag.Parse()

	ctx, stop := signal.NotifyContext(context.Background(), syscall.SIGINT, syscall.SIGTERM)
	defer stop()

	s := &http.Server{
		Handler:     handler.New(),
		Addr:        fmt.Sprintf("0.0.0.0:%d", *port),
		ReadTimeout: readTimeout,
	}
	wg := sync.WaitGroup{}

	wg.Add(1)
	go func() {
		defer wg.Done()
		if err := s.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("Listen: %s\n", err)
		}
	}()

	<-ctx.Done()
	ctx, cancel := context.WithTimeout(context.Background(), shutdownLimit)
	defer cancel()
	if err := s.Shutdown(ctx); err != nil {
		log.Println("Server forced to shutdown: ", err)
	}
	defer wg.Wait()
}
