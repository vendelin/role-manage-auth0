package util

import (
	"log"
	"os"
)

func Must(descr string, err error) {
	if err != nil {
		log.Printf("%s: %s\n", descr, err)
		os.Exit(1)
	}
}
