package util

import (
	"math/rand"
	"strings"
	"time"
)

const randChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789%!"
const (
	randIdxBits = 6                  // 6 bits to represent a letter index
	randIdxMask = 1<<randIdxBits - 1 // All 1-bits, as many as letterIdxBits
	randIdxMax  = 63 / randIdxBits   // # of letter indices fitting in 63 bits
)

func RandString(n int) string {
	randSrc := rand.NewSource(time.Now().UnixNano())
	sb := strings.Builder{}
	sb.Grow(n)
	// A src.Int63() generates 63 random bits, enough for letterIdxMax characters!
	for i, cache, remain := n-1, randSrc.Int63(), randIdxMax; i >= 0; {
		if remain == 0 {
			cache, remain = randSrc.Int63(), randIdxMax
		}
		if idx := int(cache & randIdxMask); idx < len(randChars) {
			sb.WriteByte(randChars[idx])
			i--
		}
		cache >>= randIdxBits
		remain--
	}

	return sb.String()
}
