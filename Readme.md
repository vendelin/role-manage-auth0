# role-manage-auth0
role-manage-auth0 is a Proof of Concept to manage Ath0 users, roles and permissions, written in Golang

The project uses `task` to make your life easier. If you're not familiar with Taskfiles you can take a look at [this quickstart guide](https://taskfile.dev/).

## Setup
Create a Free Tier Auth0 account.

For the details, log into Auth0, in Applications/Applications there should be a default one already created with name `API Explorer Application`. Settings there have:
`Domain`, `Client ID` and `Client Secret` to `.env` file in the repo root with a content like:

```
AUTH0_DOMAIN=<value>
AUTH0_CLIENT_ID=<value>
AUTH0_CLIENT_SECRET=<value>
AUTH0_DEFAULT_RESOURCE_SERVER=<value>
```

The default resource server is only for simplifying permission creation. Create a new custom API in Applications/API, and copy `API Audience` here. The default `Auth0 Management API` System API wont work for these purposes.

To be able to run the code in this repo, you need to set the following permissions in `Auth0 Management API` in Applications/APIs:
```
read:users, update:users, delete:users, create:users, read:resource_servers, update:resource_servers, read:roles, update:roles, delete:roles, create:roles
```

## Run
`task runServer`
Then go to http://localhost:8080/docs

## Change openapi spec
If you want to make API changes, do them in `api/openapi/auth0-management.yaml` then run `task generate`

## Lint
`task lint`
